import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {PageOneComponent} from './components/page-one/page-one.component';
import {PageThreeComponent} from './components/page-three/page-three.component';
import {PageTwoComponent} from './components/page-two/page-two.component';

const routes: Routes = [
  { path: 'page-one', component: PageOneComponent},
  { path: 'page-two', component: PageTwoComponent},
  { path: 'page-three', component: PageThreeComponent},
  // more routes here
  { path: '',   redirectTo: '/page-one', pathMatch: 'full' }, // redirect to `link-one` as a default page
  { path: '**', component: PageNotFoundComponent } // route for a 404 page
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
